﻿using System;
using System.Data;
using System.Diagnostics;
namespace HelloApp
{
    public static class Task30
    {
        public static void Run()
        {
            const int startBound = 10;
            const int stopBound = 1000000;
            const int step = 10;

            for (int i = startBound; i <= stopBound; i *= step)
            {
                CalcExecutionTimeAggoritmsSort(i);
            }
            Console.ReadKey();
        }

        private static void CalcExecutionTimeAggoritmsSort(int count)
        {
            Stopwatch sw = new Stopwatch();

            int[] data = RandomData(count);

            CalcExecutionTimeBaseSort((int[])data.Clone(), count, sw);
            CalcExecutionTimeBubbleSort((int[])data.Clone(), count, sw);
            CalcExecutionTimeInsertSort((int[])data.Clone(), count, sw);
            CalcExecutionTimeMergeSort((int[])data.Clone(), count, sw);
            CalcExecutionTimeHeapSort((int[])data.Clone(), count, sw);
            CalcExecutionTimeQuickSort((int[])data.Clone(), count, sw);

            Console.WriteLine($"End Execution {count} count;");
            Console.WriteLine();
        }

        private static void CalcExecutionTimeBaseSort(int[] data, int count, Stopwatch sw)
        {
            sw.Start();
            Array.Sort(data);
            sw.Stop();
            Console.WriteLine($"Base Sort : {count}");
            PrintResultTimeAlgoritm(sw);
        }

        private static void CalcExecutionTimeBubbleSort(int[] data, int count, Stopwatch sw)
        {
            sw.Start();
            BublleSort(data);
            sw.Stop();
            Console.WriteLine($"Bubble Sort : {count}");
            PrintResultTimeAlgoritm(sw);
        }

        private static void CalcExecutionTimeInsertSort(int[] data, int count, Stopwatch sw)
        {
            sw.Start();
            InsertSort(data);
            sw.Stop();
            Console.WriteLine($"Insert Sort : {count}");
            PrintResultTimeAlgoritm(sw);
        }

        private static void CalcExecutionTimeMergeSort(int[] data, int count, Stopwatch sw)
        {
            sw.Start();
            MergeSort(data, 0, data.Length - 1);
            sw.Stop();
            Console.WriteLine($"Merge Sort : {count}");
            PrintResultTimeAlgoritm(sw);
        }

        private static void CalcExecutionTimeHeapSort(int[] data, int count, Stopwatch sw)
        {
            sw.Start();
            HeapSort(data);
            sw.Stop();
            Console.WriteLine($"Heap Sort : {count}");
            PrintResultTimeAlgoritm(sw);
        }

        private static void CalcExecutionTimeQuickSort(int[] data, int count, Stopwatch sw)
        {
            sw.Start();
            QuickSort(data, 0, data.Length - 1);
            sw.Stop();
            Console.WriteLine($"Quick Sort : {count}");
            PrintResultTimeAlgoritm(sw);
        }

        private static void PrintResultTimeAlgoritm(Stopwatch sw)
        {
            TimeSpan ts = sw.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}:{3:00}",
                ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);           
            Console.WriteLine($"{elapsedTime}");

            sw.Reset();
            elapsedTime.Remove(0);           
        }

        private static int[] RandomData(int indexCount)
        {
            int[] array = new int[indexCount];
            Random rnd = new Random();
            for(int i = 0; i < indexCount; i++)
            {
                array[i] = rnd.Next(1, 50);
            }
            return array;
        }

        public static void BublleSort(int[] data)
        {
            for (int i = 0; i < data.Length - 1; i++)
            {
                for (int j = 0; j < data.Length - 1 - i; j++)
                {
                    if (data[j] > data[j + 1])
                    {
                        int temp = data[j];
                        data[j] = data[j + 1];
                        data[j + 1] = temp;
                    }
                }
            }
        }

        public static void InsertSort(int[] data)
        {
            for (int i = 1; i < data.Length - 1; i++)
            {
                int temp = data[i];
                int j = i;
                while (j > 0 && temp < data[j - 1])
                {
                    data[j] = data[j - 1];
                    j--;
                }
                data[j] = temp;
            }
        }

        public static void MergeSort(int[] data, int firstIndex, int lastIndex)
        {
            int i;

            if (firstIndex < lastIndex)
            {
                i = (firstIndex + lastIndex) / 2;
                MergeSort(data, firstIndex, i);
                MergeSort(data, i + 1, lastIndex);
                Merge(data, i, firstIndex, lastIndex);
            }
        }

        private static void Merge(int[] data, int index, int left, int right)
        {
            int i, j, k;

            int countLeft = index - left + 1;
            int countRight = right - index;

            int[] leftArray = new int[countLeft + 1];
            int[] rightArray = new int[countRight + 1];

            for (i = 0; i < countLeft; i++)
            {
                leftArray[i] = data[left + i];
            }

            for (j = 1; j <= countRight; j++)
            {
                rightArray[j - 1] = data[index + j];
            }

            leftArray[countLeft] = int.MaxValue;
            rightArray[countRight] = int.MaxValue;

            i = 0;
            j = 0;

            for (k = left; k <= right; k++)
            {
                if (leftArray[i] < rightArray[j])
                {
                    data[k] = leftArray[i];
                    i++;
                }
                else
                {
                    data[k] = rightArray[j];
                    j++;
                }
            }
        }

        public static void HeapSort(int[] arr)
        {
            for (int i = arr.Length / 2 - 1; i >= 0; i--)
            {
                Down(arr, i, arr.Length);
            }

            for (int i = arr.Length - 1; i >= 0; i--)
            {
                Swap(arr, 0, i);
                Down(arr, 0, i);
            }
        }

        public static void QuickSort(int[] data, int left, int right)
        {
            if (left >= right)
            {
                return;
            }

            int pivotIndex = GetPivotIndex(data, left, right);
            QuickSort(data, left, pivotIndex - 1);
            QuickSort(data, pivotIndex + 1, right);

            return;
        }

        private static int GetPivotIndex(int[] data, int left, int right)
        {
            int countPoint = left - 1;

            for (int i = left; i <= right; i++)
            {
                if (data[i] < data[right])
                {
                    countPoint++;
                    Swap(data, countPoint, i);
                }
            }
            countPoint++;
            Swap(data, countPoint, right);
            return countPoint;
        }

        private static void Down(int[] data, int root, int size)
        {
            int l = root * 2 + 1;
            int r = l + 1;
            int x = root;

            if (l < size && data[x] < data[l])
            {
                x = l;
            }
            if (r < size && data[x] < data[r])
            {
                x = r;
            }
            if (x == root)
            {
                return;
            }

            Swap(data, x, root);
            Down(data, x, size);
        }

        private static void Swap(int[] array, int a, int b)
        {
            int x = array[a];
            array[a] = array[b];
            array[b] = x;
        }
    }
}

