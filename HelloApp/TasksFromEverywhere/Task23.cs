﻿using System;
namespace HelloApp
{
    public static class Task23
    {
        public static void Run()
        {
            Console.Write("Введите число: ");
            int init = Convert.ToInt32(Console.ReadLine());

             int value  = Factorial(init);

            Console.WriteLine($"Факториал цисла {init} равен: {value}");
            Console.ReadKey();
        }

        public static int Factorial(int i)
        {
            if(i == 1)
            {
                return 1;
            }

            return i * Factorial(i - 1);
        }
    }
}

