﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloApp
{
    public class Task9
    {
        public static void Run()
        {
            Console.Write("Введите первое высказывание: ");
            bool firstExpression = Convert.ToBoolean(Convert.ToByte(Console.ReadLine()));

            Console.Write("Введите второе высказывание: ");
            bool secondExpression = Convert.ToBoolean(Convert.ToByte(Console.ReadLine()));

            bool denial = !firstExpression;
            bool сonjunction = firstExpression & secondExpression;
            bool disjunction = firstExpression | secondExpression;
            bool disjunctionStrict = firstExpression ^ secondExpression;
            bool implication = !firstExpression | secondExpression;
            bool equivalence = (!firstExpression & !secondExpression) | (firstExpression & secondExpression);

            Console.WriteLine($" Отрицание-{denial}\n Конъюнкция-{сonjunction}\n Дизъюнкция-{disjunction}\n Дизъюнкция (строгая)-{disjunctionStrict}\n Импликация-{implication}\n nЭквивалентность-{equivalence}");

            Console.ReadKey();
        }
    }
}
