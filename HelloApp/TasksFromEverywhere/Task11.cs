﻿using System;
namespace HelloApp
{
    public class Task11
    {
        public static void Run()
        {
            double result;
            Console.Write("Введите x: ");
            double x = Convert.ToDouble(Console.ReadLine());

            Console.Write("Введите y: ");
            double y = Convert.ToDouble(Console.ReadLine());

            double expression1 = Math.Pow(x, 2);
            double expression2 = (4 * x * y) + 1;

            if (expression1 > expression2)
            {
                result = Math.Log(x + y);
            }
            else if (expression1 < expression2)
            {
                result = Math.Tan(1 / (x * y));
            }
            else 
            {
                result = Math.Exp(Math.Pow(x,y) + Math.Sqrt(Math.Sin(x)));
            }
            Console.WriteLine($"Результат {result} ");
        }

    }
}
