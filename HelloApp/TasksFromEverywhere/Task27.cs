﻿using System;
using System.Linq;
using System.Text;

namespace HelloApp
{
    public static class Task27
    {
        public static void Run()
        {
            const string str = "abcab";
            int n = str.Length;
            StringBuilder sb = new StringBuilder();

            int countAdjacentSubstr = CountSubstrs(str, 0, n - 1, n, sb);
            string read = sb.ToString();
            sb.Clear();

            string adjacentSubstr = SearchAllSubs(read, str, sb, out var result);
            read = sb.ToString();

            Console.WriteLine($"Всего подстрок: {result}");
            Console.WriteLine($"Все подстроки: {read}");
            Console.WriteLine($"Количество подстрок с одинаковыми символы на границах: {countAdjacentSubstr}");
            Console.WriteLine($"Подстроки с одинаковыми символами на границах: {adjacentSubstr}");
        }

        private static string SearchAllSubs(string searchString, string text, StringBuilder sb, out int countAllSubstr)
        {
            string[] str = searchString.Split('|').Distinct().ToArray();
            countAllSubstr = 0;
            string result = string.Empty;
            
            foreach (string s in str)
            {
                bool success = int.TryParse(s, out int index);
                if (success)
                {
                    int i = index / 10, j = index % 10;
                   
                    sb.Append($"{text.Substring(i,j)}, ");
                    countAllSubstr++;
                    var checkString = text.Substring(i, j);
                    int n = checkString.Length;
                    
                    if (checkString[0] == checkString[n-1])
                    {
                        result += $"{text.Substring(i, j)}, ";
                    }
                }              
            }

            return result;
        }

        private static int CountSubstrs(string str, int i, int j, int n, StringBuilder sb)
        {
            if (n == 1)
            {
                sb.Append($"{i}{n}|");
                return 1;
            }

            if (n <= 0)
            {
                return 0;
            }

            int res = CountSubstrs(str, i + 1, j, n - 1, sb)
                      + CountSubstrs(str, i, j - 1, n - 1, sb)
                      - CountSubstrs(str, i + 1, j - 1, n - 2, sb);

            if (str[i] == str[j])
            {
                sb.Append($"{i}{n}|");
                res++;
            }

            sb.Append($"{i}{n}|");

            return res;
        }
    }
}
