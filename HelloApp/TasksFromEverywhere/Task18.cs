﻿using System;


namespace HelloApp
{
    public static class Task18
    {
        private const int Row = 10;
        private const int Col = 12;

        public static void Run()
        {
            int[,] data = CreateMatrix(Row, Col);
            int[,] saddlePoints = FindSadlePoint(data, Row, Col);          
            
            PrintDataSadlePoint(data, saddlePoints);
            Console.ReadKey();
        }

        /// <summary>
        /// Fills the matrix with random values m specified by rows and columns.
        /// </summary>
        /// <param name="row">The row count of matrix.</param>
        /// <param name="col">The column count of matrix.</param>
        /// <returns>Returns a matrix with random values</returns>
        private static int[,] CreateMatrix(int row, int col)
        {
            int[,] randomData = new int[row, col];
            Random rnd = new Random();

            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < col; j++)
                {
                    int value = rnd.Next(0, 1000);
                    randomData[i, j] = value;
                }
            }
            return randomData;
        }

        /// <summary>
        /// Finds saddle points in table(or matrix).
        /// </summary>
        /// <param name="matrix">Array for search saddle points.</param>
        /// <param name="row">>Number of rows of the output array.</param>
        /// <param name="col">Number of columns of the output array.</param>
        /// <returns>Returns a matrix with saddle points</returns>
        private static int[,] FindSadlePoint(int[,] matrix, int row, int col)
        {
            int[,] saddlePoints = new int[row, col];

            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                int rowMinValue = i;
                int colMinValue = 0;
                int rowMaxValue = i;
                int colMaxValue = 0;
                                
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (matrix[i, j] < matrix[rowMinValue, colMinValue])
                    {
                        colMinValue = j;
                        rowMinValue = i;
                    }

                    if (matrix[i, j] > matrix[rowMaxValue, colMaxValue])
                    {
                        colMaxValue = j;
                        rowMaxValue = i;
                    }
                }

                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if ((matrix[i, j] == matrix[rowMinValue, colMinValue] && CheckThatPointIsMinInColumn(matrix, i, j)) ||
                        (matrix[i, j] == matrix[rowMaxValue, colMaxValue] && CheckThatPointIsMaxInColumn(matrix, i, j)))
                    {
                        saddlePoints[i, j] = 1;
                    }
                }
            }         
            
            return saddlePoints;
        }

        /// <summary>
        /// Checks for the maximum value in the column.
        /// </summary>
        /// <param name="matrix">Array to check.</param>
        /// <param name="rowPoint">Array row counter.</param>
        /// <param name="colPoint">Array column counter.</param>
        /// /// <returns>Return <code>True</code>value is maximal<code>false</code>value is minimal</returns>
        private static bool CheckThatPointIsMinInColumn(int[,] matrix,int rowPoint, int colPoint)
        {
            for (int row = 0; row < matrix.GetLength(0); row++)
            {
                if (matrix[row, colPoint] > matrix[rowPoint, colPoint])
                {
                    return false;
                }                
            }

            return true;
        }

        /// <summary>
        /// Checks for the minimum value in the column.
        /// </summary>
        /// <param name="matrix">Array to check.</param>
        /// <param name="rowPoint">Array row counter.</param>
        /// <param name="colPoint">Array column counter.</param>
        /// <returns>Return <code>True</code>value is minimal<code>false</code>value is maximal</returns>
        private static bool CheckThatPointIsMaxInColumn(int[,] matrix, int rowPoint, int colPoint)
        {
            for (int row = 0; row < matrix.GetLength(0); row++)
            {
                if (matrix[row, colPoint] < matrix[rowPoint, colPoint])
                {
                    return false;
                }               
            }

            return true;
        }

        /// <summary>
        /// Outputs to the console of an array with an underscore in red with the value "1" of the compared array.
        /// </summary>
        /// <param name="matrix">Array to output.</param>
        /// <param name="saddlePoints">Array for comparison.</param>
        private static void PrintDataSadlePoint(int[,] matrix, int[,] saddlePoints)
        {
            for(int i = 0; i < matrix.GetLength(0); i++)
            {
                for(int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (saddlePoints[i,j] == 1)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.Write($"{matrix[i, j]}\t");
                        Console.ResetColor();
                    }
                    else
                    {
                        Console.Write($"{matrix[i, j]}\t");
                    }
                }

                Console.WriteLine();
            }
        }       
    }
}
