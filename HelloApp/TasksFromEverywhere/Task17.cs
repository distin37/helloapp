﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;


namespace HelloApp
{
    public static class Task17
    {
        public static void Run()
        {
            string test = "Привет ";
            
            for(uint count = 10; count <= 100000; count *= 10)
            {
                PrintRes(test, count);
            }
            Console.ReadKey();
        }

        public static void PrintRes(string test, uint count)
        {
            string res1 = CalcConcatString(test, count);
            Console.WriteLine(res1);
            string res = CalcStringBild(test, count);
            Console.WriteLine(res);
        }

        public static string CalcStringBild(string test, uint count)
        {           
            Stopwatch sw = new Stopwatch();
            StringBuilder sb = new StringBuilder();
            
            sw.Start();
            for (uint i = 0; i < count; i++)
            {
                sb.Append(test);
            }
            sw.Stop();

            TimeSpan ts = sw.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds / 10);
            
            sb.Clear();
            sb.Append($"StringBulder").Append(' ',8).Append($"{count} ({elapsedTime})\n");
            sb.Append('_', 29);
            sb.AppendLine();
            test = sb.ToString();
            return test;
        }

        public static string CalcConcatString(string test, uint count)
        {
            string result = "";
            Stopwatch sw = new Stopwatch();
            StringBuilder sb = new StringBuilder();
            
            sw.Start();
            for (uint i = 0; i < count; i++)
            {
                result+= test;
            }
            sw.Stop();
            
            TimeSpan ts = sw.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds / 10);
            
            sb.Append($"ConcatinationSrings {count} ({elapsedTime})\n");
            test = sb.ToString();
            return test;
        }
    }
}
