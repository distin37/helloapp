﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloApp
{
    public class Task10
    {
        public static void Run()
        {
            const double rubleExchangeRate = 1;
            const double dollarExchangeRate = 71.7846;
            const double poundExchangeRate = 98.3449;
            const double euroExchangeRate = 83.3347;
            const string fieldRub = "rub";
            const string fieldUsd = "usd";
            const string fieldGbp = "gpb";
            const string fieldEur = "eur";

            Console.Write($"Введите валюту для конвертации \"{fieldRub}\", \"{fieldUsd}\", \"{fieldGbp}\", \"{fieldEur}\" : ");
            string fromCurrency = Console.ReadLine();

            Console.Write($"Введите валюту для конвертирования \"{fieldRub}\", \"{fieldUsd}\", \"{fieldGbp}\", \"{fieldEur}\" : ");
            string toCurrency = Console.ReadLine();

            Console.Write("Введите сумма расчета : ");
            double sumCurrency = Convert.ToDouble(Console.ReadLine());
       
            double fromRate = fromCurrency switch
            {
                fieldRub => rubleExchangeRate,
                fieldUsd => dollarExchangeRate,
                fieldGbp => poundExchangeRate,
                fieldEur => euroExchangeRate,
                _ => -1,
            };

            double toRate = toCurrency switch
            {
                fieldRub => rubleExchangeRate,
                fieldUsd => dollarExchangeRate,
                fieldGbp => poundExchangeRate,
                fieldEur => euroExchangeRate,
                _ => -1,
            };
            
            double resultCurrency = sumCurrency * (fromRate / toRate);
            double resultCurrencyRounding = Math.Round(resultCurrency, 2);

            Console.WriteLine($"Сумма равна {resultCurrencyRounding} ");

            Console.ReadKey();
        }
    }
}
