﻿using System;
namespace HelloApp
{
    public static class Task24
    {
        public static void Run()
        {
            const int s = 0;

            Console.Write("Введите число N: ");
            int value = Convert.ToInt32(Console.ReadLine());            
            Console.WriteLine($"Результат: {CollatzFunction(value, s)}");
            Console.ReadKey();
        }

        private static int CollatzFunction(int value, int s)
        {
            if(value == 1)
            {
                Console.WriteLine($"Кол-во операций коллатца: {s}");
                return value;
            }

            s++;
            int result = value % 2 == 0 ? CollatzFunction(value / 2, s) : CollatzFunction((3 * value) + 1, s);

            return s % 2 == 0 ? value + result : value * result;
        }
    }
}

