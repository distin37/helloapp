﻿using System;
using System.Collections;
using System.Linq;
namespace HelloApp
{
	public static class Task29
	{
		public static void Run()
		{
            object[][] jaggedArray = new object[5][];

            FillJaggedArray(jaggedArray);
            PrintArray(jaggedArray);

            Console.ReadKey();
        }

		private static void FillJaggedArray(object[][]jaggedArray)
		{            
            string[] stringArray = new string[] { "one", "two" };
            double[] doubleArray = new double[] { 0.234, 31.2456 };
            bool[] boolArray = new bool[] { true, false };
            char[] charArray = new char[] { '1', 'F' };
            int[] intArray = new int[] { 1, 2 };

            jaggedArray[0] = stringArray.Cast<object>().ToArray();
            jaggedArray[1] = intArray.Cast<object>().ToArray();
            jaggedArray[2] = doubleArray.Cast<object>().ToArray();
            jaggedArray[3] = boolArray.Cast<object>().ToArray();
            jaggedArray[4] = charArray.Cast<object>().ToArray();
        }

        private static void PrintArray(object[][] jaggedArray)
        {
            for(int i = 0; i < jaggedArray.Length; i++)
            {
                switch (i)
                {
                    case 0:
                        string[] strings = jaggedArray[i].Cast<string>().ToArray();
                        Console.WriteLine($"{strings.GetType()} {string.Join("", strings)}");
                        break;
                    case 1:
                        int[] ints = jaggedArray[i].Cast<int>().ToArray();
                        Console.WriteLine($"{ints.GetType()} {ints[0] + ints[1]}");
                        break;
                    case 2:
                        double[] doubles = jaggedArray[i].Cast<double>().ToArray();
                        Console.WriteLine($"{doubles.GetType()} {doubles[0] + doubles[1]}");
                        break;
                    case 3:
                        bool[] bools = jaggedArray[i].Cast<bool>().ToArray();
                        Console.WriteLine($"{bools.GetType()} {bools[0] || bools[1]}");
                        break;
                    case 4:
                        char[] chars = jaggedArray[i].Cast<char>().ToArray();
                        Console.WriteLine($"{chars.GetType()} {string.Join("", chars)}");
                        break;
                }                
            }         
        }
	}
}

