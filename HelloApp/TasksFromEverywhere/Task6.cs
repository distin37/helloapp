﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloApp
{
    public class Task6
    {
        public static void Run()
        {
            Console.WriteLine("Введите a:");
            double a = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Введите b:");
            double b = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Введите c:");
            double c = Convert.ToDouble(Console.ReadLine());

            double oneMetod = Math.Abs((3 * a) + c);
            double twoMetod = Math.Log(oneMetod);
            double x = (Math.Pow(twoMetod, 2) / 5) + (Math.Sin(Math.PI*c));
           
            double y = Math.Abs(1 - a) * (Math.Sin((Math.PI / 4) + b));

            Console.WriteLine($"x = {x}, y = {y}");
            Console.ReadKey();
        }
    }
}
