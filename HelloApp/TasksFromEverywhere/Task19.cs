﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace HelloApp
{
    public static class Task19
    {
        public static void Run()
        {
            string text = CheckText();
            SearchPolyndrom(text);
            Console.ReadKey();
        }

        public static string CheckText()
        {
            Console.WriteLine("Введите текст для проверки на полиндром");
            string text = Console.ReadLine().ToLower();

            return text;
        }

        public static void SearchPolyndrom(string textSearch)
        {
            Regex search = new Regex(@"\W");
            string regText = search.Replace(textSearch, "");

            char[] charArray = regText.ToCharArray();
            Array.Reverse(charArray);
            string checkText = new string(charArray);

            if(regText == checkText)
            {
                Console.WriteLine("Текст является палиндромом");
            }
            else
            {
                Console.WriteLine("Текст не является палиндромом");
            }            
        }
    }
}
