﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloApp
{
    public static class Task20
    {
        public static void Run()
        {
            string initWord = "апельсин";

            initWord = initWord
                .MoveSubstring(5, 1)
                .MoveSubstring(1, 1)
                .MoveSubstring(0, 1)
                .MoveSubstring(3, 2, true)
                .MoveSubstring(0, 3);
              
            Console.WriteLine(initWord);
            Console.ReadKey();
        }
       
        public static string MoveSubstring(this string @string, int startIndex, int lengthIndex, bool withReverse = false)
        {
            string result = @string.Substring(startIndex, lengthIndex);

            if (withReverse)
            {
                result = result.Reverse();
            }

            @string = @string.Insert(8, result).Remove(startIndex, lengthIndex);

            return @string;
        }

        static string Reverse(this string @string)
        {          
            char[] arr = @string.ToCharArray();
            Array.Reverse(arr);
            @string = new string(arr);

            return @string;
        }
    }
}
