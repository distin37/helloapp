﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloApp
{
    public class Task16
    {
        public static void Run()
        {
            int[,] data = CreateRandomData(15, 15);
            string result = new string("");
            FindMinElement(data,out result);
            PrintData(result);                     
            Console.ReadKey();
        }

        /// <summary>
        /// Reads the rows count and columns count from console.
        /// </summary>
        /// <param name="row">The row count of matrix.</param>
        /// <param name="col">The column count of matrix.</param>
        public static int[,] CreateRandomData(int row, int col)
        {
            int[,] randomData = new int[row, col];
            Random rnd = new Random();

            for(int i = 0; i < row; i++)
            {
                for(int j = 0; j < col; j++)
                {
                    int value = rnd.Next(0,1000);
                    randomData[i,j] = value;
                }
            }
            return randomData;
        }

        /// <summary>
        /// Used to find and output the index of the smallest number of an array.
        /// </summary>
        /// <param name="findData">Array to search for a number.</param>
        /// <param name="finalResult">Write all values to a string.</param>
        public static void FindMinElement(int[,] findData, out string finalResult)
        {
            StringBuilder data = new StringBuilder();
            StringBuilder resultMinCount = new StringBuilder();
            int result = int.MaxValue;
                                    
            for(int i = 0; i < findData.GetLength(0); i++)
            {
                for( int j = 0; j < findData.GetLength(1); j++)
                {
                    data.Append($"{findData[i,j]}\t");
                    
                    if(findData[i,j] < result)
                    {
                        result=findData[i,j];
                        resultMinCount.Clear();          
                    }  
                    
                    if(findData[i,j] == result)
                    {
                        resultMinCount.Append($" {i},{j}");
                    }
                }
                data.AppendLine();
            }
            data.AppendLine();
            data.Append($"Минимальное значение элемента в массиве: {result}\n");
            data.Append($"Индексы массивов: {resultMinCount}");
            finalResult = data.ToString();
        }

        /// <summary>
        /// Output to the console array.
        /// </summary>
        /// <param name="result">Output to console.</param>
        public static void PrintData(string result)
        {
            Console.WriteLine(result);
        }
    }
}
