﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloApp
{
    public static class Task21
    {
        public static void Run()
        {
            int m = 10, n = 10;

            double[] x = new double[n];
            double[] y = new double[m];

            x = CreateRandomData(x, n);
            y = CreateRandomData(y, m);

            double result = AddEquations(x, y, n, m);

            Console.WriteLine($"Формула S равна = {result}");
            Console.ReadKey();
        }

        private static double AddEquations(double[] x,double[] y, int n, int m)
        {
            double result = CalcLeftExpression(x, n) + CalcRightExpression(y, m);

            return result;
        }

        private static double[] CreateRandomData(double[] randomData, int lengthArray)
        {
            Random rnd = new Random();

            for(int i = 0; i < lengthArray; i++)
            {
                int value = rnd.Next(0, 10);
                randomData[i] = value;
            }

            return randomData;
        }

        private static double CalcLeftExpression(double[] data, int n, int i = 1)
        {
            double res = 0;
            for (; i < n; i++)
            {
                res += (Math.Pow(Math.Sin(data[i]), 4));                
            }

            Math.Sqrt(res);

            return res;
        }

        private static double CalcRightExpression(double[] data, int m, int k = 1)
        {
            double res = 1;
            for(; k < m; k++)
            {
                res *= (Math.Cos(Math.Pow(data[k], 2)));               
            }

            return res; 
        }
    }  
}

