﻿using System;
using System.Text.RegularExpressions;

namespace HelloApp
{
    public static class Task26
    {
        public static void Run()
        {
            bool result = true;
            string text = "";

            Console.WriteLine("Введите текст:");
            CheckLatinLovercaseLetters(out text);
            result = IsPalindrome(text);
            CheckResult(result);
            Console.ReadKey();
        }

        private static void CheckLatinLovercaseLetters(out string text)
        {
            text = "";
            bool success = false;
            
            var regex = new Regex(@"[a-z]");

            while (!success)
            {
                text = Console.ReadLine();

                if (regex.IsMatch(text))
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Неверный ввод \nВведите текст:");
                    continue;
                }
            }
        }

        public static bool IsPalindrome(string text)
        {
            if (text.Length <= 1)
            {
                return true;
            }
            if (text[0] != text[text.Length - 1])
            {
                return false;
            }
            else
            {
                return IsPalindrome(text.Substring(1, text.Length - 2));
            }
        }

        private static void CheckResult(bool res)
        {
            if (res)
            {
                Console.WriteLine("Yes");
            }
            else
            {
                Console.WriteLine("No");
            }
        }

    }
}

