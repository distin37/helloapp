﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloApp
{
    public class Task3
    {
        public static void Run()
        {
            const int porkWeight = 170;
            const int onionPiece = 1;
            const int potatoWeight = 340;
            const int cheeseWeight = 85;

            Console.WriteLine("Введите О или 1 ");
            string selection = Console.ReadLine();
            switch (selection)
            {
                case "0":
                    Console.WriteLine("Введите количество порций для расчета ингридиентов");
                    int portion = Convert.ToInt32(Console.ReadLine());
                    
                    int pork = portion * porkWeight;
                    int onion = portion * onionPiece;
                    int potato = portion * potatoWeight;
                    int cheese = portion * cheeseWeight;
                    
                    Console.WriteLine($"Для приготовления {portion} порций блюда нам понадобится:");
                    Console.WriteLine($"{pork} грамм Свинины");
                    Console.WriteLine($"{onion} штуки Лука");
                    Console.WriteLine($"{potato} грамм Картофеля");
                    Console.WriteLine($"{cheese} грамм Сыра");
                    break;
                
                case "1":
                    Console.WriteLine("Введите количество грамм свинины:");
                    int countPorks = Convert.ToInt32(Console.ReadLine());

                    Console.WriteLine("Введите количество луковиц:");
                    int countOnions = Convert.ToInt32(Console.ReadLine());

                    Console.WriteLine("Введите количество грамм картофеля:");
                    int countPotatoes = Convert.ToInt32(Console.ReadLine());

                    Console.WriteLine("Введите количество грамм сыра:");
                    int countCheezes = Convert.ToInt32(Console.ReadLine());

                    int countPorkPortions = countPorks / porkWeight;
                    int countOnionPortions = countOnions / onionPiece;
                    int countPotatoPortions = countPotatoes / potatoWeight;
                    int countCheesePortions = countCheezes / cheeseWeight;
                   
                    int minCountPortions = countPorkPortions;
                    if (minCountPortions > countOnionPortions)
                    {
                        minCountPortions = countPorkPortions;
                    }
                    
                    if (minCountPortions > countOnionPortions)
                    {
                        minCountPortions = countOnionPortions;
                    }
                    
                    if (minCountPortions > countPotatoPortions)
                    {
                        minCountPortions = countPotatoPortions;
                    }

                    if (minCountPortions > countCheesePortions)
                    {
                        minCountPortions = countCheesePortions;
                    }

                    Console.WriteLine($"Получилось {minCountPortions} порций");
                    break;
            }
            Console.ReadKey();
        }
    }
}
