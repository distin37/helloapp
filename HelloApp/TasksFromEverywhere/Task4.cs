﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloApp
{
    public class Task4
    {
        public static void Run()
        {
            const int num1 = 2;
            const int num2 = 10;
            const int num3 = 7;

            Console.WriteLine("Введите первое число:");
            int firstNumber = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Введите второе число");
            int secondNumber = Convert.ToInt32(Console.ReadLine());
 
            if (secondNumber == num3)
            {
                Console.WriteLine("груша");
            }
            else if (firstNumber > num1 && firstNumber <= num2)
            {
                Console.WriteLine("яблоко");
            }
            else
            {
                Console.WriteLine("апельсин");
            }
            
            Console.ReadKey();
        }
    }
}
