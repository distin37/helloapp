﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloApp
{
    public class Task12
    {
        public static void Run()
        {
            const string firstMethod = "for";
            const string secondMethod = "do";
            const string thirdMethod = "while";
            const string y = "y";
            bool success;
            string result;
            string method;
            uint limit;
            
            do
            {
                do
                {               
                    Console.Write("Введите положительное число и 0: ");

                    success = uint.TryParse(Console.ReadLine(), out  limit);
                    if (!success)
                    {
                        Console.Write("Неверный ввод. ");
                    }                   
                }
                while (!success);
               
                do
                {
                    Console.Write($"Введите метод расчёта, один из: {firstMethod}, {secondMethod}, {thirdMethod} ");

                    method = Console.ReadLine().ToLower();

                    success = method == firstMethod || method == secondMethod || method == thirdMethod;
                    if (!success)
                    {
                        Console.Write("Неверный ввод. ");
                    }                                                     
                }
                while (!success);
               
                Console.Write("Результат: ");
                switch (method)
                {
                    case firstMethod:
                        for (int i = 0; limit >= i; i++)
                        {
                            Console.Write(i == limit ? $"{i}." : $"{i}, ");
                        }
                        break;
                    case secondMethod:
                        int d = 0;
                        do
                        {
                            Console.Write(d == limit ? $"{d}." : $"{d}, ");
                            d++;
                        }
                        while (limit >= d);
                        break;
                    case thirdMethod:
                        int w = 0;
                        while (limit >= w)
                        {
                            Console.Write(w == limit ? $"{w}." : $"{w}, ");
                            w++;
                        }
                        break;                 
                }      
                Console.Write("\nПожалуйста нажмите клавишу Y для повтора программы, любую клавищу что бы выйти из программы: ");
                result = Console.ReadLine().ToLower();
            }
            while (result == y);
        }
    }
}
