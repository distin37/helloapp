﻿using System;
using System.Diagnostics;
namespace HelloApp
{
    public static class Task31
    {
        public static void Run()
        {
            const int startBound = 10;
            const int stopBound = 1000000;
            const int step = 10;

            for (int i = startBound; i <= stopBound; i *= step)
            {
                CalcExecutionTimeSearchAlgorithms(i);
            }
            Console.ReadKey();
        }

        private static void CalcExecutionTimeSearchAlgorithms(int count)
        {
            Stopwatch sw = new Stopwatch();
            Random rnd = new Random();

            int[] array = SortRandomData(count, rnd);
            int searchValue = rnd.Next(0, count);

            CalcExecutionTimeLinearSearchAlgorithms(array, searchValue, count, sw);
            CalcExecutionTimeBinarySearchAlgorithms(array, searchValue, count, sw);

            Console.WriteLine($"End Execution {count} count;");
            Console.WriteLine();
        }

        private static void CalcExecutionTimeLinearSearchAlgorithms(int[] array, int value, int count, Stopwatch sw)
        {
            sw.Start();
            LinearSearch(array, value);
            sw.Stop();
            Console.WriteLine($"Linear Search: {count}");
            PrintResultTimeAlgoritm(sw);
        }

        private static void CalcExecutionTimeBinarySearchAlgorithms(int[] array, int value, int count, Stopwatch sw)
        {
            sw.Start();
            BinarySearchRecursive(array, value, 0, array.Length - 1);
            sw.Stop();
            Console.WriteLine($"Binary Search: {count}");
            PrintResultTimeAlgoritm(sw);
        }

        private static int[] SortRandomData(int indexCount, Random rnd)
        {
            int[] array = new int[indexCount];

            for (int i = 0; i < array.Length - 1; i++)
            {
                array[i] = rnd.Next(0, indexCount);
            }

            Array.Sort(array);
            return array;
        }

        private static void PrintResultTimeAlgoritm(Stopwatch sw)
        {
            TimeSpan ts = sw.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}:{3:00}",
                ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
            Console.WriteLine($"{elapsedTime}");

            sw.Reset();
            elapsedTime.Remove(0);
        }

        private static void LinearSearch(int[] array, int value)
        {
            for (int i = 0; i < array.Length - 1; i++)
            {
                if (array[i] == value)
                {
                    return;
                }
            }
        }

        private static void BinarySearchRecursive(int[] array, int value, int firstIndex, int lastIndex)
        {
            int middle = (firstIndex + lastIndex) / 2;

            if (firstIndex > lastIndex || array[middle] == value)
            {
                return;
            }

            if (array[middle] > value)
            {
                BinarySearchRecursive(array, value, firstIndex, middle - 1);
            }
            else
            {
                BinarySearchRecursive(array, value, middle + 1, lastIndex);
            }
        }
    }
}

