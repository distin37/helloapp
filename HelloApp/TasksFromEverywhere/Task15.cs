﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloApp
{
    public class Task15
    {
        public static void Run()
        {
            ReadSideSquare(out ushort value);
            Console.WriteLine();
            PrintSquare(value);
            Console.ReadKey();
        }

        /// <summary>
        /// Reads and checks the square size from the console.
        /// </summary>
        /// <param name="value">Square size.</param>
        public static void ReadSideSquare (out ushort value)
        {
            bool success;
            Console.WriteLine("Пожалуйста введите сторону площади:");

            do
            {
                success = ushort.TryParse(Console.ReadLine(), out value);
                if (!success || value <= 1)
                {
                    Console.WriteLine("Неверный ввод");
                    success = false;
                }

            } while (!success);
        }

        /// <summary>
        /// Print a hollow square to the console.
        /// </summary>
        /// <param name="value">Specifies the size of the sides of the square.</param>
        public static void PrintSquare(ushort value)
        {
            StringBuilder topBottomBuilder = new StringBuilder().Insert(0, " *",value);
            StringBuilder bodyBuilder = new StringBuilder($" *{new string(' ', (value - 2) * 2)} *\n");
            StringBuilder stars = new StringBuilder();

            stars.Append($"{topBottomBuilder}\n");
            
            for (ushort i = 0; i < value - 2; i++)
            {
                stars.Append(bodyBuilder);
            }

            stars.Append(topBottomBuilder);
            Console.WriteLine(stars.ToString());
        }
    }
}
