﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloApp
{
    public class Task2
    {
        public static void Run()
        {
            Console.WriteLine("Введите радиус окружности");
            double radius = Convert.ToDouble(Console.ReadLine());
            double circumference = Math.PI * (radius * 2);
            Console.WriteLine($"Длина окружности равна {circumference}!");
            Console.ReadKey();    
        }
    }
}
