﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloApp
{
    public class Task5
    {
        public static void Run()
        {
            Console.WriteLine("Введите количество яблок которые получил Петя:");
            int numberOfPeterApples = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Введите количество яблок которые получила Лена:");
            int numberOfElenaApples = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Введите цену покупки яблок которы были выброшены:");
            double priceOfDiscardedApples = Convert.ToDouble(Console.ReadLine());

            int totalNumberOfApples = numberOfElenaApples + numberOfPeterApples;
            double transferredApplesToPeter = (numberOfElenaApples * 2 / 3) + numberOfPeterApples;
            double numberOfDiscardedApples = transferredApplesToPeter * 2 / 5;
            double sumApples = (priceOfDiscardedApples / numberOfDiscardedApples) * totalNumberOfApples;
            
            double result = Math.Round(sumApples, 2);
            Console.WriteLine($"Полная стоимость яблок составляет: {result}");

            Console.ReadKey();
        }
    }
}
