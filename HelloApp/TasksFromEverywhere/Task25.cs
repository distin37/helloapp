﻿using System;
namespace HelloApp
{
    public static class Task25
    {
        public static void Run()
        {
            const int m = 2;
            const int n = 2;

            Console.WriteLine($"Вывод: {AkkermanFunction(n, m)}");
            Console.ReadKey();
        }

        private static int AkkermanFunction(int m, int n)
        {

            if (m == 0)
            {
                return n + 1;
            }
            else if (m > 0 && n == 0)
            {
                return AkkermanFunction(m - 1, 1);
            }
            else
            {
                return AkkermanFunction(m - 1, AkkermanFunction(m, n - 1));
            }
        }
    }
}

