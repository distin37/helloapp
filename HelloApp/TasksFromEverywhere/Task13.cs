﻿using System;
using System.Text.RegularExpressions;
namespace HelloApp
{
	public class Task13
	{
        public static void Run()
        {
            bool sucess = false;
            ushort year = 0;
            ushort mounth = 0;
            ushort day = 0;
            ushort daysCount = 0;
            string dateFalse = "Неверный ввод, ведите дату: ";

            ushort[] dateMounth = new ushort[] { 0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30 };
            string[] dateStringArray = new string[3];
            Console.WriteLine("Введите дату:");

            while (!sucess)
            {           
                string dateString = Console.ReadLine();
                var regex = new Regex(@"[0-9]{2}\.[0-9]{2}\.[0-9]{4}");
                if (dateString.Length == 10 && regex.IsMatch(dateString))
                {
                    dateStringArray = dateString.Split('.');
                }
                else
                {
                    Console.Write(dateFalse);
                    continue;
                }

                sucess = ushort.TryParse(dateStringArray[1], out mounth);
                if (sucess && mounth < 12)
                {
                    mounth = (ushort)(mounth - 1);
                }
                else
                {
                    Console.Write(dateFalse);
                    continue;
                }

                ushort dayInMounth = 0;
                for (int i = 0; i < mounth; i++)
                {
                    dayInMounth = dateMounth[i];
                }

                sucess = ushort.TryParse(dateStringArray[0], out day);
                if (!sucess && day > dayInMounth)
                {
                    sucess = false;
                    Console.Write(dateFalse);
                    continue;
                }

                sucess = ushort.TryParse(dateStringArray[2], out year);
                if (!sucess)
                {
                    Console.Write(dateFalse);
                    continue;
                }

            }
             
            for (int i = 0; i < mounth; i++)
            {
                daysCount += dateMounth[i];
            }

            daysCount += day;

            if ((year % 4 == 0) && (daysCount > 28))
            {
                daysCount--;
            }
            
            daysCount = (ushort)(daysCount % 8);
            

            if ((daysCount == 0) && (daysCount == 3) && (daysCount == 4) )
            {
                Console.WriteLine("Не Пьет");
            }
            else
            {
                Console.WriteLine("Пьет");
            }
            
            Console.ReadKey();
        }
	}
}
