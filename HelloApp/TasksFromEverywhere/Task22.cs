﻿using System;

namespace HelloApp
{
    public static class Task22
    {
        public static void Run()
        {
            int i  = 10 , j = 10 , m  = 10, n = 10;
            double result;

            i = RandomValue(i);
            j = RandomValue(j);

            double[,] array = RandomArray(m, n);
            
            result = SolvEquation(i, j, m, n, array);

            Console.WriteLine($"Формула S равна = {result}");
            Console.ReadKey();
        }

        private static int RandomValue(int value)
        {
            Random rnd = new Random();
            value = rnd.Next(1, 5);

            return value;
        }

        private static double[,] RandomArray(int row, int col)
        {
            Random rnd = new Random();
            double[,] array = new double[row, col];

            for(int i = 0; i < row; i++)
            {
                for(int j = 0; j < col; j++)
                {
                    int value = rnd.Next(0, 10);
                    array[i, j] = value;
                }
            }

            return array;
        }

        private static double SolvEquation(int i,int j,int m ,int n, double[,] data)
        {
            double result = 0;

            for(; 0 <= i && i <=  m; i++)
            {
                for(; 0 < j && j < n; j++)
                {
                    result += LeftSideEquation(data, i, j) + RightSideEquation(i, j, m);
                }
            }

            return result;
        }

        private static double LeftSideEquation(double[,] p, int i, int j)
        {
            double result = p[i, j];
            result = Math.Exp(i * j);
            return result;
        }

        private static double RightSideEquation(int i = 0, int j  = 0, int m = 0)
        {
            double result = 1;
            for (; i <= j; i++)
            {
                result *= Math.Pow((Math.Sqrt((2 * i) / (j + 1))),5) + Math.Pow(Scsh(m),-1);
            }

            return result;
        }

        public static double Scsh(double d)
        {
            d = 2 / ((Math.Pow(Math.E, d)) - (Math.Pow(Math.E, -d)));
            return d;
        }
    }    
}

