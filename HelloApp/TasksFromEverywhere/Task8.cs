﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloApp
{
    public class Task8
    {
        public static void Run()
        {
            Console.Write("Введите координату x : ");
            double xCoordinate = Convert.ToDouble(Console.ReadLine());

            Console.Write("Введите координату y : ");
            double yCoordinate = Convert.ToDouble(Console.ReadLine());

            if (xCoordinate > 0 && yCoordinate > 0)
                Console.WriteLine("Точка с координатами лежит в первой четверти");
            if (xCoordinate < 0 && yCoordinate > 0)
                Console.WriteLine("Точка с координатами лежит во второй четверти");
            if (xCoordinate < 0 && yCoordinate < 0)
                Console.WriteLine("Точка с координатами лежит в третьей четверти");
            if (xCoordinate > 0 && yCoordinate < 0)
                Console.WriteLine("Точка с координатами лежит в четвертой четверти");
            if (xCoordinate == 0 && yCoordinate == 0)
                Console.WriteLine("Точка с координатами лежит в начале");

            Console.ReadKey();
        }
    }
}
