﻿using System;
using System.Collections.Generic;
using System.Text;
using static HelloApp.Task14;

namespace HelloApp
{
    public static class Task28
    {
        public static void Run()
        {
            //bool success= true;
            uint rings;

            Console.WriteLine(" Ханойские Башни \n Введите на какую башню будет построение? B/C ?");
            string res = Console.ReadLine().ToLower();

            Console.Write("Введите колличество колец (от 1 до 10): ");

            CheckValue(out rings);

            if (res == "c")
            {
                Hanoi(rings, "А", "B", "С");
            }
            else
            {
                Hanoi(rings, "А", "C", "B");
            }

            Console.ReadKey();
        }

        public static void CheckValue(out uint rings, TestFunction testFunction = null)
        {
            bool success;
            
            do
            {
                success = uint.TryParse(Console.ReadLine(), out rings);

                if (!success || (rings == 0 || rings > 10))
                {
                    Console.WriteLine("Неверный ввод");       
                    testFunction?.Invoke(rings);
                }
            }
            while (!success || (rings == 0 || rings > 10));
        }

        public static void Hanoi(uint n, string a, string b, string c)
        {   
            if(n >= 1)
            {
                Hanoi(n - 1, a, c, b);
                Console.WriteLine($"Диск перемещается с {a} на {c}");
                Hanoi(n - 1, b, a, c);
            }          
        }

        public delegate void TestFunction(uint result);
    }
}
