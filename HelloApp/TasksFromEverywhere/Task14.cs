﻿using System;
using System.Diagnostics;


namespace HelloApp
{
    public class Task14
    {
        public static void Run()
        {
            ReadMatrixSize(out uint row, out uint column);
            uint[,] data = CreateSpiralData(row, column);
            PrintData(data);

            Console.ReadKey();
        }

        /// <summary>
        /// Reads the rows count and columns count from console.
        /// </summary>
        /// <param name="rowCount">The row count of matrix.</param>
        /// <param name="columnCount">The column count of matrix.</param>
        private static void ReadMatrixSize(out uint rowCount, out uint columnCount)
        {
            Console.WriteLine("Пожалуйста введите количество столбцов:");
            ReadMatrixValue(out rowCount);

            Console.WriteLine("Пожалуйста введите количество колонок:");
            ReadMatrixValue(out columnCount);                 
        }

        /// <summary>
        /// Reads a uint number from console.
        /// </summary>
        /// <param name="value">Returns an uint number.</param>
        /// <param name="testFunction">Function for tests.</param>
#if DEBUG || TEST
        public static void ReadMatrixValue(out uint value, TestFunction testFunction = null)
#endif
#if RELEASE
        public static void ReadMatrixValue(out uint value)
#endif
        {
            bool success;

            do
            {
                success = uint.TryParse(Console.ReadLine(), out value);
                if (!success || value == 0)
                {
                    Console.WriteLine("Неверный ввод");
                    success = false;
#if DEBUG || TEST
                    testFunction?.Invoke(value);
#endif
                }

            } while (!success);
        }

        /// <summary>
        /// Fills a two-dimensional matrix in spiral order.
        /// </summary>
        /// <param name="rowCount">The row count of matrix.</param>
        /// <param name="columnCount">The column count of matrix.</param>
        /// <returns>Returns a filled matrix with data.</returns>
        public static uint[,] CreateSpiralData(uint rowCount, uint columnCount)
        {
            uint x = 0; // A cursor that shows a dot on the top horizontal line of matrix.
            uint y = rowCount - 1; // A cursor that shows a dot on the top or left vertical line of matrix.
            uint z = columnCount - 1; // A cursor that shows a dot on the bottom horizontal line or right vertical line of matrix.

            uint countCells = rowCount * columnCount;
            uint filledCellsCount= 1;
            uint[,] data = new uint[rowCount, columnCount];

            while (filledCellsCount <= countCells)
            {
                for (uint i = x; i <= z; i++)
                {
                    data[x, i] = filledCellsCount++;
                }
                if (filledCellsCount > countCells)
                    break;
                
                for (uint j = x + 1; j < y; j++)
                {
                    data[j, z] = filledCellsCount++;
                }
                if (filledCellsCount > countCells)
                    break;
                
                for (uint i = z; i > x; i--)
                {
                    data[y, i] = filledCellsCount++;
                }
                if (filledCellsCount > countCells)
                    break;
                
                for (uint j = y; j > x; j--)
                {
                    data[j, x] = filledCellsCount++;
                }
                x++;
                y--;
                z--;
            }
            return data;
        }

        /// <summary>
        /// Prints values line by line.
        /// </summary>
        /// <param name="data">Values to print.</param>
        public static void PrintData(uint[,] data)
        {
#if !TEST
            Debug.Assert(data != null, "data is null");
            Debug.Assert(data.Length > 0, "data is empty");
#endif        
            if (data == null || data.Length == 0)
            {
                Console.WriteLine("Sorry something went wrong, but we will fix it soon!");
                return;
            }
            
            int row = data.GetUpperBound(0) + 1;
            int colums = data.GetUpperBound(1) + 1;

            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < colums; j++)
                {
                    Console.Write($"{data[i, j]}\t");
                }
                Console.WriteLine();

            }

        }

        public delegate void TestFunction(uint result);
    }
}
