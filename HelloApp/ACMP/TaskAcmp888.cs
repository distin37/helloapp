﻿using System;

namespace HelloApp.ACMP
{
    public static class TaskAcmp888
    {
        public static void Run()
        {
            const long numbPoints = 3;
            const long correctAnswer = 1;

            long count = numbPoints;
            long summPoints = 0;
            long numbTask = long.Parse(Console.ReadLine());
            
            string[] ansv = Console.ReadLine().Split(' ');
            
            for (long i = 0; i < numbTask; i++)
            {
                if (int.Parse(ansv[i]) == correctAnswer)
                {
                    summPoints += count++;
                }
                else
                {
                    count = numbPoints <= (count - numbPoints) ? count - numbPoints : numbPoints;                   
                }
            }
            Console.WriteLine(summPoints);
        }
    }
}
