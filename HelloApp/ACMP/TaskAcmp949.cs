﻿using System;


namespace HelloApp.ACMP
{
    public static class TaskAcmp949
    {
        public static void Run()
        {
            string[] tokens = Console.ReadLine().Split(' ');           
            SearchFibonacciTermsInReverse((int.Parse(tokens[0])) - 1, int.Parse(tokens[1]), int.Parse(tokens[2])); 
        }

        private static void SearchFibonacciTermsInReverse(int index, int previousValue, int currentValue)
        {
            if (index == 0)
            {
                Console.WriteLine($"{previousValue} {currentValue}");
                return; 
            }
            int res = currentValue - previousValue;
            
            SearchFibonacciTermsInReverse(index - 1, res, previousValue);
        }        
    }
}
