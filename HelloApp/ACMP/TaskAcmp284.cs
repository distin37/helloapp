﻿using System;

namespace HelloApp.ACMP
{
    public static class TaskAcmp284
    {
        public static void Run()
        {
            int arraySize = int.Parse(Console.ReadLine());
            string[] array = new string[arraySize];
            int minValue, maxValue;

            array = Console.ReadLine().Split(' ');
            int countSubArr = int.Parse(Console.ReadLine());

            for(int i = 0; i < countSubArr; i++)
            {
                string[] valuesSubArr = Console.ReadLine().Split(' ');
                minValue = int.Parse(valuesSubArr[0]);
                maxValue = int.Parse(valuesSubArr[1]);

                for(int j = minValue - 1;j < maxValue; j++)
                {                    
                    Console.Write(minValue == maxValue ? $"{array[j]}" : $"{array[j]} ");
                }
                Console.WriteLine();
            }          
        }
    }
}
