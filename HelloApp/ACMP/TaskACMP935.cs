﻿using System;


namespace HelloApp.ACMP
{
    public static class TaskAcmp935
    {
        public static void Run()
        {
            const int boardLenght = 8;
            
            string[] tokens = Console.ReadLine().Split(' ');

            int[,] chessBoard  = CreateChessBoard(boardLenght);
            bool result = CheckValueInArr(chessBoard, tokens);

            Console.WriteLine(result ? "YES" : "NO");                 
        }

        private static int[,] CreateChessBoard(int rowCol)
        {
            int[,] arr = new int[rowCol, rowCol];
            
            for(int i = 0; i < arr.GetLength(0); i++)
            {
                for(int j = 0; j < arr.GetLength(1); j++)
                {
                    arr[i,j] = (i + j) % 2 == 0 ? 1 : 0;
                }
            }
            return arr;
        }

        private static bool CheckValueInArr(int[,] arr, string[] value)
        {
            return (arr[int.Parse(value[0]) - 1, int.Parse(value[1]) - 1] ==
                    arr[int.Parse(value[2]) - 1, int.Parse(value[3]) - 1]);
        }
    }
}
