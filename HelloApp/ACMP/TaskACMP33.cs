﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloApp.ACMP
{
    public static class TaskAcmp33
    {
        public static void Run()
        {
            string[] y = (Console.ReadLine().Split(' '));
            Console.WriteLine($"{(int.Parse(y[1])) - 1} {(int.Parse(y[0])) - 1}");
        }
    }
}
