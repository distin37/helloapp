﻿using System;


namespace HelloApp.ACMP
{
    public static class TaskAcmp387
    {
        public static void Run()
        {
            int countRules = int.Parse(Console.ReadLine());

            int countString = CheckFirstAndThirdCharInString(countRules);
            Console.WriteLine(countString);

        }

        public static int CheckFirstAndThirdCharInString(int countRules)
        {
            int countString = 0;
            for(int i = 0; i < countRules; i++)
            {
                string termLine = Console.ReadLine();

                if (termLine[0] == termLine[3])
                {
                    countString++;
                }
            }
            return countString;
        }
    }
}
