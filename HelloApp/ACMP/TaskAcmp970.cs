﻿using System;


namespace HelloApp.ACMP
{
    public static class TaskAcmp970
    {
        public static void Run()
        {
            string[] tokens = Console.ReadLine().Split(' ');

            bool result = IsRearrangeableEquality(tokens);
            Console.WriteLine(result ? "YES" : "NO");
        }

        private static bool IsRearrangeableEquality(string[] equalityValues)
        {
            return (IsEqualityTrue(equalityValues, 0, 1, 2)) ||
                   (IsEqualityTrue(equalityValues, 1, 2, 0)) ||
                   (IsEqualityTrue(equalityValues, 0, 2, 1));
        }

        /// <summary>
        /// Сhecks whether the equality of two terms is true
        /// </summary>
        /// <param name="equalityValues"></param>
        /// <param name="term1">The value of the first term</param>
        /// <param name="term2">The value of the second term</param>
        /// <param name="sum">The value of the sum for checking the terms</param>
        /// <returns>Returns true if the sum is equal to the addition of two terms, otherwise it returns false</returns>
        private static bool IsEqualityTrue(string[] equalityValues, int term1, int term2, int sum)
        {
            return ((int.Parse(equalityValues[term1])) + (int.Parse(equalityValues[term2])) ==
                    (int.Parse(equalityValues[sum])));
        }
    }
}