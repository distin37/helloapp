﻿using System;

namespace HelloApp.ACMP
{
    public static class TaskAcmp929
    {
        public static void Run()
        {
            long value = long.Parse(Console.ReadLine());

            long maxValue = 6 * value;
            long minValue = SearchMinValue(value);

            Console.WriteLine($"{minValue} {maxValue}");
        }

        private static long SearchMinValue(long value) =>
             value % 6 switch
             {
                 1 => value / 6 + 6,
                 2 => value / 6 + 5,
                 3 => value / 6 + 4,
                 4 => value / 6 + 3,
                 5 => value / 6 + 2,
                 _ => value / 6
             };                  
    }
}