﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Xunit;

namespace HelloApp.Tests
{
    public class Task28Tests
    {
        [Theory]
        [MemberData(nameof(GetReadNullEmptyAndFillingString))]
        public void HanoiCheckMovementValue(uint count, string left, string midlle, string right, string expectedOutput)
        {
            var stringWriter = new StringWriter();
            Console.SetOut(stringWriter);

            Task28.Hanoi(count, left, midlle, right);

            var output = stringWriter.ToString();

            Assert.Equal(expectedOutput, output);

        }

        private void FunctionForTestBadFirstInput(uint result, string goodInput, StringWriter stringWriter)
        {
            Assert.Equal(0u, result);
            Assert.Equal($"Неверный ввод{Environment.NewLine}", stringWriter.ToString());
            var stringReader = new StringReader(goodInput);
            Console.SetIn(stringReader);
        }

        [Theory]
        [MemberData(nameof(GetInputTestData))]
        public void CheckValueWithBadFirstInput(string[] inputs, uint expectedGoodInput)
        {
            var stringWriter = new StringWriter();
            Console.SetOut(stringWriter);
            // This code is similar to the method
            var stringReader = new StringReader(inputs[0]);
            Console.SetIn(stringReader);
            var testFunction = new Task28.TestFunction((goodInput) => FunctionForTestBadFirstInput(goodInput, inputs[1], stringWriter));

            Task28.CheckValue(out uint result, testFunction);

            Assert.Equal(expectedGoodInput, result);
        }

        public static IEnumerable<object[]> GetInputTestData()
        {
            return new List<object[]>
            {
                new object[] { new string[] {"", "1"},  1u },
                new object[] { new string[] {" ", "1"}, 1u },
                new object[] { new string[] {"Text", "1"}, 1u },
                new object[] { new string[] {"-1", "1"}, 1u },
                new object[] { new string[] {"0", "1"}, 1u },
                new object[] { new string[] {"4294967296", "1"}, 1u }, // uint max value + 1
            };
        }

        public static IEnumerable<object[]> GetReadNullEmptyAndFillingString()
        {
            return new List<object[]>
            {
                new object[] { 0, "A", "B", "C", ""},
                new object[] { 1, "A", "B", "C", "Диск перемещается с A на C\r\n"},
                new object[] { 2, "A", "B", "C", "Диск перемещается с A на B\r\nДиск перемещается с A на C\r\nДиск перемещается с B на C\r\n"},
            };
        }
    }
}
