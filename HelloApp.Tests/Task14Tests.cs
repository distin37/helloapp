using System;
using System.Collections.Generic;
using System.IO;
using Xunit;

namespace HelloApp.Tests
{
    public class Task14Tests
    {
        [Theory]
        [MemberData(nameof(GetData))]
        public void CreateSpiralData_CheckSpiralFilling_True(uint rowCount, uint columnCount, uint[,] expectedSpiralData)
        {
            uint[,] result = Task14.CreateSpiralData(rowCount, columnCount);

            Assert.Equal(expectedSpiralData, result);
        }

        [Theory]
        [InlineData("1", 1)]
        [InlineData("7", 7)]
        public void ReadMatrixValue_CheckForNumber_True(string input, uint expected)
        {
            var stringReader = new StringReader(input);
            Console.SetIn(stringReader);

            Task14.ReadMatrixValue(out uint result);

            Assert.Equal(expected, result);
        }

        private void FunctionForTestBadFirstInput(uint result, string goodInput, StringWriter stringWriter)
        {
            Assert.Equal(0u, result);
            Assert.Equal($"Неверный ввод{Environment.NewLine}", stringWriter.ToString());
            var stringReader = new StringReader(goodInput);
            Console.SetIn(stringReader);
        }

        [Theory]
        [MemberData(nameof(GetInputTestData))]
        public void ReadMatrixValueWithBadFirstInput(string[] inputs, uint expectedGoodInput)
        {
            var stringWriter = new StringWriter();
            Console.SetOut(stringWriter);
            // This code is similar to the method
            var stringReader = new StringReader(inputs[0]);
            Console.SetIn(stringReader);
            var testFunction = new Task14
                .TestFunction((goodInput) => FunctionForTestBadFirstInput(goodInput, inputs[1], stringWriter));
            Task14.ReadMatrixValue(out uint result, testFunction);

            Assert.Equal(expectedGoodInput, result);
        }

        public static IEnumerable<object[]> GetInputTestData()
        {
            return new List<object[]>
            {
                new object[] { new string[] {"", "1"},  1u },
                new object[] { new string[] {" ", "1"}, 1u },
                new object[] { new string[] {"Text", "1"}, 1u },
                new object[] { new string[] {"-1", "1"}, 1u },
                new object[] { new string[] {"0", "1"}, 1u },
                new object[] { new string[] {"4294967296", "1"}, 1u }, // uint max value + 1
            };
        }

        [Theory]
        [MemberData(nameof(GetReadData))]
        public void PrintData_OutputOfLinesToConsOle_CorrectConclusion(uint[,] spiralData, string expectedOuput)
        {
            var stringWriter = new StringWriter();
            Console.SetOut(stringWriter);

            Task14.PrintData(spiralData);

            var output = stringWriter.ToString();

            Assert.Equal(expectedOuput, output);
        }

        [Theory]
        [MemberData(nameof(GetReadNullAndEmptyData))]
        public void PrintData_OutputOfLinesToConsOle_NullAndEmptyConclusion(uint[,] spiralData, string expectedOuput)
        {
            var stringWriter = new StringWriter();
            Console.SetOut(stringWriter);

            Task14.PrintData(spiralData);

            var output = stringWriter.ToString();

            Assert.Equal(expectedOuput, output);
        }

        public static IEnumerable<object[]> GetData()
        {
            return new List<object[]>
            {
                new object[] { 2, 6, new uint [,] { { 1, 2, 3, 4, 5, 6 }, { 12, 11, 10, 9, 8, 7 } } },
                new object[] { 2, 1, new uint [,] { { 1 }, { 2 } } },
                new object[] { 1, 3, new uint [,] { { 1, 2, 3} } },
                new object[] { 0,0, new uint [0, 0] },
                new object[] { 0,1, new uint [0, 1] },
                new object[] { 1,0, new uint [1, 0] },
                new object[] { 1,1, new uint [1,1] { { 1 } } },
                new object[] { 3, 3, new uint [,] { { 1, 2, 3 }, { 8, 9, 4 }, { 7, 6, 5 } } },
                new object[] { 3, 5, new uint [,] { { 1, 2, 3, 4, 5 }, { 12, 13, 14, 15, 6 }, { 11, 10, 9, 8, 7 } } },
            };
        }

        public static IEnumerable<object[]> GetReadData()
        {
            return new List<object[]>
            {
                new object[] {new uint [2,3], "0\t0\t0\t\r\n0\t0\t0\t\r\n"},
                new object[] {new uint[,] { { 1, 2, 3 }, { 4, 5, 6 } }, "1\t2\t3\t\r\n4\t5\t6\t\r\n"},
                new object[] {new uint[,] { { 10, 45, 1, 3}, { 4, 5, 6, 11 } }, "10\t45\t1\t3\t\r\n4\t5\t6\t11\t\r\n" },
            };
        }

        public static IEnumerable<object[]> GetReadNullAndEmptyData()
        {
            return new List<object[]>
            {
                new object[] {null, "Sorry something went wrong, but we will fix it soon!\r\n" },
                new object[] {new uint[,] { { } }, "Sorry something went wrong, but we will fix it soon!\r\n" },
            };
        }
    }
}